package se.yrgo.drinkspiration.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.yrgo.drinkspiration.data.CocktailIngredientRepo;
import se.yrgo.drinkspiration.data.CocktailRepo;
import se.yrgo.drinkspiration.data.IngredientRepo;
import se.yrgo.drinkspiration.model.CockTailIngredientDTO;
import se.yrgo.drinkspiration.model.Cocktail;
import se.yrgo.drinkspiration.model.CocktailIngredient;
import se.yrgo.drinkspiration.model.Ingredient;
import se.yrgo.drinkspiration.model.Volume;

/*
 *  TODO: The @Transactional annotation is mostly needed for the CommandLineRunner (AutoFillData) to 
 *  work properly. When going "live" on a real DB (postgreSQL) we should, again, evaluate the need for 
 *  this annotation. 
 */

@Service
@Transactional
public class CocktailIngredientServiceImpl implements CocktailIngredientService{
	
	@Autowired
	IngredientRepo ingredientRepo;
	
	@Autowired
	CocktailIngredientRepo repo;
	
	@Autowired
	CocktailRepo cocktailRepo;

	@Override
	public CockTailIngredientDTO createCocktailIngredient(String ingredientName, int amount, String volumeUnit, Long cocktailId) {

//		Find ingredient in database
		Ingredient ingredient = ingredientRepo.findIngredientByIngredientNameIgnoreCase(ingredientName);
		
		Volume cocktailUnit = null;
		
//		Check for valid volumeUnit
		for (Volume volume : Volume.values()) {
			if(volume.name().equalsIgnoreCase(volumeUnit)) {
				cocktailUnit = volume;
			}
		}
		
//		Check that Ingredient and VolumeUnit is valid. If not throw IllegalArgumentException 
		if(ingredient == null || cocktailUnit == null ) {
			throw new IllegalArgumentException("No valid Ingredient and/or Volume unit");
		}
		
//		Create CocktailIngredient
		CocktailIngredient cocktailIngredient = new CocktailIngredient(amount, ingredient, cocktailUnit);
		
//		Get the cocktailReferred to and add the ingredient. 
		Cocktail cocktail = cocktailRepo.getOne(cocktailId);
		cocktail.addCocktailIngredient(cocktailIngredient);
			
		CocktailIngredient savedCocktailIngredient =  repo.save(cocktailIngredient);
		return convertToDTO(savedCocktailIngredient);
	}
	

	private CockTailIngredientDTO convertToDTO(CocktailIngredient cocktailIngredient) {
		
		CockTailIngredientDTO dto = new CockTailIngredientDTO();
		dto.setAmount(cocktailIngredient.getAmount());
		dto.setIngredient(cocktailIngredient.getIngredient());
		return dto;
		
	}
}
