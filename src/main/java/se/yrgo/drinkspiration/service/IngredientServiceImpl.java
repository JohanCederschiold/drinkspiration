package se.yrgo.drinkspiration.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import se.yrgo.drinkspiration.data.IngredientRepo;
import se.yrgo.drinkspiration.model.Ingredient;
import se.yrgo.drinkspiration.model.IngredientDTO;

@Service
public class IngredientServiceImpl implements IngredientService {
	
	private IngredientRepo repo;
	
	public IngredientServiceImpl(IngredientRepo repo) {
		super();
		this.repo = repo;
	}

	@Override
	public IngredientDTO registerNewIngredient(IngredientDTO ingredientDTO) throws DuplicateIngredientException {
		Ingredient ingredient = convertFromDTO(ingredientDTO);
		Ingredient test = repo.findIngredientByIngredientNameIgnoreCase(ingredientDTO.getIngredientName());
		if (test == null ) {
			ingredient = repo.save(ingredient);		
		} else {
			throw new DuplicateIngredientException();
		}
		return convertToDTO(ingredient);
	}

	@Override
	public List<IngredientDTO> getAllIngredients() {
		
		List<Ingredient>ingredients = repo.findAll();
		List<IngredientDTO> ingredientsToSend = new ArrayList<IngredientDTO>();
		
		for (Ingredient ingredient : ingredients) {
			ingredientsToSend.add(convertToDTO(ingredient));
		}
		
		return ingredientsToSend;
	}
	
	private Ingredient convertFromDTO (IngredientDTO ingredientDTO) {
		Ingredient ingredient = new Ingredient();
		ingredient.setIngredientName(ingredientDTO.getIngredientName());
		return ingredient;
	}
	
	private IngredientDTO convertToDTO (Ingredient ingredient) {
		IngredientDTO ingredientDTO = new IngredientDTO();
		ingredientDTO.setIngredientName(ingredient.getIngredientName());
		return ingredientDTO;
	}

}
