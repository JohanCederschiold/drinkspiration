package se.yrgo.drinkspiration.service;

import java.net.URL;
import java.util.List;

import se.yrgo.drinkspiration.model.SystembolagetAssortmentObjectDTO;
import se.yrgo.drinkspiration.model.SystembolagetStoreObject;
import se.yrgo.drinkspiration.model.SystembolagetStoreObjectDTO;

public interface SystembolagetStoreService {
	
	public List<SystembolagetStoreObject> getStoreList(SystembolagetStoreObjectDTO SystembolagetStoreObjectDTO); 
	public URL getStoreURL();
	public SystembolagetStoreObjectDTO parseStoreXML();
	public SystembolagetStoreObjectDTO parseXml(URL url);
	public void saveStoreListToDatabase(List<SystembolagetStoreObject> storeObjectList);

}
