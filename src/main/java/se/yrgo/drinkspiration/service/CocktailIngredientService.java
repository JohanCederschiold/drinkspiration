package se.yrgo.drinkspiration.service;


import se.yrgo.drinkspiration.model.CockTailIngredientDTO;

public interface CocktailIngredientService {
	
	public CockTailIngredientDTO createCocktailIngredient(String ingredient, int amount, String volumeUnit, Long cocktailId );


}
