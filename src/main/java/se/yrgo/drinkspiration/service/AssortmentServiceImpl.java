package se.yrgo.drinkspiration.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import se.yrgo.drinkspiration.data.AssortmentRepo;
import se.yrgo.drinkspiration.model.SystembolagetAssortmentObject;
import se.yrgo.drinkspiration.model.SystembolagetAssortmentObjectDTO;

@Service
public class AssortmentServiceImpl implements AssortmentService {
	
	@Autowired
	AssortmentRepo assortmentRepo;

	@Override
	public List<SystembolagetAssortmentObject> getAssortmentList(SystembolagetAssortmentObjectDTO assortmentObject) {
		List<SystembolagetAssortmentObject> assortment = new ArrayList<SystembolagetAssortmentObject>();
		for (SystembolagetAssortmentObjectDTO assortmentDTO : assortmentObject.getSortimentList()) {
			assortment.add(new SystembolagetAssortmentObject(assortmentDTO));
		}
		return assortment;
	}

	@Override
	public SystembolagetAssortmentObjectDTO parseAssortmentXML() {
		return null;
	}

	@Override
	public URL getAssortmentURL() {
		URL url;
		try {
			url = new URL("https://www.systembolaget.se/api/assortment/products/xml");
			return url;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public SystembolagetAssortmentObjectDTO parseXml(URL url) {
		JAXBContext jaxbContext;
		SystembolagetAssortmentObjectDTO assortment;
		try {
			jaxbContext = JAXBContext.newInstance(SystembolagetAssortmentObjectDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			assortment = (SystembolagetAssortmentObjectDTO) jaxbUnmarshaller.unmarshal(url);
			return assortment;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public void fillDatabaseWithAssortment(List<SystembolagetAssortmentObject> assortmentObject) {
		for(SystembolagetAssortmentObject databaseAssortmentObject : assortmentObject) {
			assortmentRepo.save(databaseAssortmentObject);
		}		
	}
	
	@Scheduled(cron = "0 30 5 * * *")
	public void fillDatabaseScheduledJob() {
		URL url = getAssortmentURL();
		SystembolagetAssortmentObjectDTO assortmentDTO = parseXml(url);
		List<SystembolagetAssortmentObject> assortmentObject = getAssortmentList(assortmentDTO);
		fillDatabaseWithAssortment(assortmentObject);
	}

}
