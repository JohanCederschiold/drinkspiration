package se.yrgo.drinkspiration.service;

import java.net.URL;
import java.util.List;

import se.yrgo.drinkspiration.model.SystembolagetAssortmentObjectDTO;
import se.yrgo.drinkspiration.model.SystembolagetAssortmentObject;

public interface AssortmentService {
	
	public List<SystembolagetAssortmentObject> getAssortmentList(SystembolagetAssortmentObjectDTO systembolagetAssortmentObjectDTO); 
	public URL getAssortmentURL();
	public SystembolagetAssortmentObjectDTO parseAssortmentXML();
	public SystembolagetAssortmentObjectDTO parseXml(URL url);
	public void fillDatabaseWithAssortment(List<SystembolagetAssortmentObject> assortmentObject);
	
}
