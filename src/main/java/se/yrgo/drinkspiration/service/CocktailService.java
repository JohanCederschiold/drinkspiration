package se.yrgo.drinkspiration.service;

import java.util.List;
import java.util.Set;

import se.yrgo.drinkspiration.model.CockTailIngredientDTO;
import se.yrgo.drinkspiration.model.CocktailDTO;

public interface CocktailService {
	
	public CocktailDTO addCocktail (CocktailDTO cocktail);
	public CocktailDTO getCocktailById(Long id);
	public CocktailDTO getCocktailByName(String name);
	public Set<CockTailIngredientDTO> getIngredientsForCocktail(Long id);
	public CocktailDTO getRandomCocktail();
	public List<CocktailDTO> getAllCocktails();

}
