package se.yrgo.drinkspiration.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import se.yrgo.drinkspiration.data.SystembolagetStoreRepo;
import se.yrgo.drinkspiration.model.SystembolagetAssortmentObject;
import se.yrgo.drinkspiration.model.SystembolagetAssortmentObjectDTO;
import se.yrgo.drinkspiration.model.SystembolagetStoreObject;
import se.yrgo.drinkspiration.model.SystembolagetStoreObjectDTO;

@Service
public class SystembolagetStoreServiceImpl implements SystembolagetStoreService {
	
	@Autowired
	SystembolagetStoreRepo storeRepo;

	@Override
	public List<SystembolagetStoreObject> getStoreList(SystembolagetStoreObjectDTO SystembolagetStoreObjectDTO) {
		List<SystembolagetStoreObject> assortment = new ArrayList<SystembolagetStoreObject>();
		for (SystembolagetStoreObjectDTO assortmentDTO : SystembolagetStoreObjectDTO.getButikerList()) {
			assortment.add(new SystembolagetStoreObject(assortmentDTO));
		}
		return assortment;
	}

	@Override
	public URL getStoreURL() {
		URL url;
		try {
			url = new URL("https://www.systembolaget.se/api/assortment/stores/xml");
			return url;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public SystembolagetStoreObjectDTO parseStoreXML() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SystembolagetStoreObjectDTO parseXml(URL url) {
		JAXBContext jaxbContext;
		SystembolagetStoreObjectDTO assortment;
		try {
			jaxbContext = JAXBContext.newInstance(SystembolagetStoreObjectDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			assortment = (SystembolagetStoreObjectDTO) jaxbUnmarshaller.unmarshal(url);
			return assortment;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void saveStoreListToDatabase(List<SystembolagetStoreObject> storeObjectList) {
		for (SystembolagetStoreObject storeObject : storeObjectList) {
			storeRepo.save(storeObject);
		}
	}
	
	@Scheduled(cron = "0 0 6 * * *")
	public void fillDatabaseScheduledJob() {
		URL url = getStoreURL();
		SystembolagetStoreObjectDTO storeDTO = parseXml(url);
		List<SystembolagetStoreObject> storeObject = getStoreList(storeDTO);
		saveStoreListToDatabase(storeObject);
	}
	
	

}
