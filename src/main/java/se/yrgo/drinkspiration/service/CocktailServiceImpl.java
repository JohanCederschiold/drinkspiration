package se.yrgo.drinkspiration.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.stereotype.Service;

import se.yrgo.drinkspiration.data.CocktailRepo;
import se.yrgo.drinkspiration.model.CockTailIngredientDTO;
import se.yrgo.drinkspiration.model.Cocktail;
import se.yrgo.drinkspiration.model.CocktailDTO;
import se.yrgo.drinkspiration.model.CocktailIngredient;

@Service
public class CocktailServiceImpl implements CocktailService {
	
	private CocktailRepo repo;
	private CocktailIngredientService cocktailIngredientService;

	
	public CocktailServiceImpl(CocktailRepo repo, CocktailIngredientService cocktailIngredientService) {
		this.repo = repo;
		this.cocktailIngredientService = cocktailIngredientService;
	}

	@Override
	public CocktailDTO addCocktail(CocktailDTO cocktailDTO) {
		
		Cocktail cocktail = convertFromDTO(cocktailDTO);
		Cocktail savedCocktail = repo.save(cocktail);
		CocktailDTO returnDTO = convertToDTO(savedCocktail);
		return returnDTO;
	}
	
	@Override
	public Set<CockTailIngredientDTO> getIngredientsForCocktail(Long id) {
		
		Cocktail requestedCocktail = repo.getOne(id);
		Set<CocktailIngredient> ingredients = requestedCocktail.getCocktailIngredient();
		Set<CockTailIngredientDTO> ingredientDTOs = new HashSet<>();
		
		for (CocktailIngredient ci : ingredients) {
			ingredientDTOs.add(convertToDTO(ci));
		}
		
		return ingredientDTOs;
	}
	
	private Cocktail convertFromDTO (CocktailDTO cocktailDTO) {
		
		Cocktail cocktail = new Cocktail();
		cocktail.setDescription(cocktailDTO.getDescription());
		cocktail.setName(cocktailDTO.getName());
		return cocktail;
	}
	
	private CocktailDTO convertToDTO(Cocktail cocktail) {
		CocktailDTO dto = new CocktailDTO();
		dto.setId(cocktail.getId());
		dto.setName(cocktail.getName());
		dto.setDescription(cocktail.getDescription());
		return dto;
	}


	
	private CockTailIngredientDTO convertToDTO(CocktailIngredient cocktailIngredient) {
		
		CockTailIngredientDTO dto = new CockTailIngredientDTO();
		dto.setAmount(cocktailIngredient.getAmount());
		dto.setIngredient(cocktailIngredient.getIngredient());
		return dto;
		
	}
	
	public List<CocktailDTO> getAllCocktails(){
		List<Cocktail> allCocktails = repo.findAll();
		List<CocktailDTO> allCocktailsDTO = new ArrayList<CocktailDTO>();
		for(Cocktail ct : allCocktails) {
			allCocktailsDTO.add(convertToDTO(ct));
		}
		return allCocktailsDTO;
	}
	
	public CocktailDTO getRandomCocktail() {
		List<CocktailDTO> allCocktails = getAllCocktails();
		Random random = new Random();
		CocktailDTO randomCocktail = allCocktails.get(random.nextInt(allCocktails.size()));
		return randomCocktail;
	}

	@Override
	public CocktailDTO getCocktailById(Long id) {
		return convertToDTO(repo.getOne(id));
	}

	@Override
	public CocktailDTO getCocktailByName(String name) {
		return convertToDTO(repo.findCocktailByNameIgnoreCase(name));
	}

}
