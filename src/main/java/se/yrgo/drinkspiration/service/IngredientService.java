package se.yrgo.drinkspiration.service;

import java.util.List;

import se.yrgo.drinkspiration.model.IngredientDTO;

public interface IngredientService {
	
	public IngredientDTO registerNewIngredient(IngredientDTO ingredient) throws DuplicateIngredientException;
	public List<IngredientDTO> getAllIngredients();

}
