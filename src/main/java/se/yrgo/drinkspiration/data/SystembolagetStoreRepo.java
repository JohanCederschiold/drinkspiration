package se.yrgo.drinkspiration.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import se.yrgo.drinkspiration.model.SystembolagetStoreObject;

@Repository
public interface SystembolagetStoreRepo extends JpaRepository <SystembolagetStoreObject, Long> {

}
