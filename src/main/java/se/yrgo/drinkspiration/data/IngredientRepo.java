package se.yrgo.drinkspiration.data;

import org.springframework.data.jpa.repository.JpaRepository;

import se.yrgo.drinkspiration.model.Ingredient;

public interface IngredientRepo extends JpaRepository<Ingredient, Long> {
	
//	public Ingredient findIngredientByIngredientName(String name);
	public Ingredient findIngredientByIngredientNameIgnoreCase(String name);
	
}
