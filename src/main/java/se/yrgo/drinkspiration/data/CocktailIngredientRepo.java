package se.yrgo.drinkspiration.data;

import org.springframework.data.jpa.repository.JpaRepository;

import se.yrgo.drinkspiration.model.CocktailIngredient;

public interface CocktailIngredientRepo extends JpaRepository<CocktailIngredient, Long> {

}
