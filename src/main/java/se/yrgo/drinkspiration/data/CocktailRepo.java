package se.yrgo.drinkspiration.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import se.yrgo.drinkspiration.model.Cocktail;

@Repository
public interface CocktailRepo extends JpaRepository<Cocktail, Long> {
	public Cocktail findCocktailByNameIgnoreCase(String name);

}
