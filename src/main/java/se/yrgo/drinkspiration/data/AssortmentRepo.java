package se.yrgo.drinkspiration.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import se.yrgo.drinkspiration.model.SystembolagetAssortmentObject;

@Repository
public interface AssortmentRepo extends JpaRepository<SystembolagetAssortmentObject, Long> { 

}
