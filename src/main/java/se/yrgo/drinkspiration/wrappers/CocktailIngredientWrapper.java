package se.yrgo.drinkspiration.wrappers;

public class CocktailIngredientWrapper {
	
	int amount;
	String ingredient;
	String unit;
	Long cocktailId;
	
	
	public CocktailIngredientWrapper() {
		super();
	}


	


	public CocktailIngredientWrapper(int amount, String ingredient, String unit, Long cocktailId) {
		this.amount = amount;
		this.ingredient = ingredient;
		this.unit = unit;
		this.cocktailId = cocktailId;
	}





	public int getAmount() {
		return amount;
	}


	public void setAmount(int amount) {
		this.amount = amount;
	}


	public String getIngredient() {
		return ingredient;
	}


	public void setIngredient(String ingredient) {
		this.ingredient = ingredient;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	


	public Long getCocktailId() {
		return cocktailId;
	}





	public void setCocktailId(Long cocktailId) {
		this.cocktailId = cocktailId;
	}
	
	





	@Override
	public String toString() {
		return "CocktailIngredientWrapper [amount=" + amount + ", ingredient=" + ingredient + ", unit=" + unit + "]";
	}
	
	
	
	

}
