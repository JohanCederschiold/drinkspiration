package se.yrgo.drinkspiration.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import se.yrgo.drinkspiration.model.SystembolagetAssortmentObject;
import se.yrgo.drinkspiration.model.SystembolagetAssortmentObjectDTO;
import se.yrgo.drinkspiration.service.AssortmentService;

@RestController
public class AssortmentController {
	
	@Autowired 
	AssortmentService assortmentService;
	
	@Scheduled(cron = "0 0 4 1/1 * ?")
	@GetMapping("/fillDatabaseWithAssortment")
	public void setSortiment() throws JAXBException, MalformedURLException{
		URL url = assortmentService.getAssortmentURL();
		SystembolagetAssortmentObjectDTO assortmentDTO = assortmentService.parseXml(url);
		List<SystembolagetAssortmentObject> assortmentObject = assortmentService.getAssortmentList(assortmentDTO);
		assortmentService.fillDatabaseWithAssortment(assortmentObject);
	}

}
