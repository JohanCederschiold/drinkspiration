package se.yrgo.drinkspiration.controller;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import se.yrgo.drinkspiration.model.CockTailIngredientDTO;
import se.yrgo.drinkspiration.model.CocktailDTO;
import se.yrgo.drinkspiration.service.CocktailIngredientService;
import se.yrgo.drinkspiration.service.CocktailService;
import se.yrgo.drinkspiration.wrappers.CocktailIngredientWrapper;

@RestController
public class CocktailController {
	
	private CocktailService service;
	private CocktailIngredientService cocktailIngredientService;

	public CocktailController(CocktailService service, CocktailIngredientService cocktailIngredient) {
		super();
		this.service = service;
		this.cocktailIngredientService = cocktailIngredient;
	}

	@PostMapping("/addcocktail")
	public CocktailDTO registerCocktail(@RequestBody CocktailDTO cocktail) {
		CocktailDTO dto = service.addCocktail(cocktail);
		return dto;
	}
	
	@GetMapping("/getCocktailId/{id}")
	public CocktailDTO getById(@PathVariable Long id) {
		return service.getCocktailById(id);
	}
	
	@GetMapping("/getCocktailName/{name}")
	public CocktailDTO getById(@PathVariable String name) {
		return service.getCocktailByName(name);
	}
	
	@PostMapping("/addcocktailingredient")
	public CockTailIngredientDTO registerCocktailIngredient(@RequestBody CocktailIngredientWrapper wrapper) {
		
		CockTailIngredientDTO cocktailIngredient = cocktailIngredientService.createCocktailIngredient(wrapper.getIngredient(), 
				wrapper.getAmount(), 
				wrapper.getUnit(),
				wrapper.getCocktailId());

		return cocktailIngredient;
	}
	
	@GetMapping("/getRandomCocktail")
	public CocktailDTO getRandomCocktail() {
		return service.getRandomCocktail();
	}
	
	@GetMapping("/getAllCocktails")
	public List<CocktailDTO>getAllCocktails(){
		return service.getAllCocktails();
	}
	
	@GetMapping("/getingredientsforcocktail/{id}")
	public Set<CockTailIngredientDTO> getCocktailIngredientsForCocktail(@PathVariable Long id) {
		Set<CockTailIngredientDTO> ingredients =  service.getIngredientsForCocktail(id);
		
		return ingredients;
	}
	

}
