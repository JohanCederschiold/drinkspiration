package se.yrgo.drinkspiration.controller;

import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import se.yrgo.drinkspiration.model.SystembolagetStoreObject;
import se.yrgo.drinkspiration.model.SystembolagetStoreObjectDTO;
import se.yrgo.drinkspiration.service.SystembolagetStoreService;

@RestController
public class SystembolagetStoreController {

	@Autowired
	SystembolagetStoreService storeService;

	@Scheduled(cron = "0 0 4 1 * ?")
	@GetMapping("/fillStore")
	public void fillDatabase() {
		URL url = storeService.getStoreURL();
		SystembolagetStoreObjectDTO storeDTO = storeService.parseXml(url);
		List<SystembolagetStoreObject> storeObjectList = storeService.getStoreList(storeDTO);
		storeService.saveStoreListToDatabase(storeObjectList);
	}
}
