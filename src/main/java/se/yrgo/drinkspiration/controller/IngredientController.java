package se.yrgo.drinkspiration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import se.yrgo.drinkspiration.model.IngredientDTO;
import se.yrgo.drinkspiration.service.DuplicateIngredientException;
import se.yrgo.drinkspiration.service.IngredientService;

@RestController
public class IngredientController {
	
	@Autowired
	IngredientService service;
	
	
	@PostMapping("/addingredient")
	public IngredientDTO registerIngredient(@RequestBody IngredientDTO ingredient) {
		
		if(ingredient.getIngredientName() == null ) {
			return null;
		}
		
		IngredientDTO reply = null;
		
		try {
			reply = service.registerNewIngredient(ingredient);
		} catch (DuplicateIngredientException e) {
			e.printStackTrace();
		}
		return reply;
	}
	
	@GetMapping("/ingredients")
	public List<IngredientDTO> getAllIngredients() {
		return service.getAllIngredients();
	}

}
