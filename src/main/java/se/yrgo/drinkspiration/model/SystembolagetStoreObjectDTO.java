package se.yrgo.drinkspiration.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ButikerOmbud")
@XmlAccessorType(XmlAccessType.FIELD)
public class SystembolagetStoreObjectDTO {
	
	private String Typ = "";
	private String Nr = "";
	private String Namn = "";
	private String Address1 = "";
	private String Address2 = "";
	private String Address3 = "";
	private String Address4 = "";
	private String Address5 = "";
	private String Telefon = "";
	private String ButiksTyp = "";
	private String Tjänster = "";
	private String SokOrd = "";
	private String Oppettider = "";
	private String RT90x = "";
	private String RT90y = "";

	@XmlElement(name = "ButikOmbud")
	private List<SystembolagetStoreObjectDTO> butiker;	

	public String getTyp() {
		return Typ;
	}

	@XmlElement
	public void setTyp(String typ) {
		Typ = typ;
	}

	public String getNr() {
		return Nr;
	}

	public void setNr(String nr) {
		Nr = nr;
	}

	

	public String getSokOrd() {
		return SokOrd;
	}

	public void setSokOrd(String sokOrd) {
		this.SokOrd = sokOrd;
	}


	public String getRT90y() {
		return RT90y;
	}

	public void setRT90y(String rT90y) {
		RT90y = rT90y;
	}
	
	public List<SystembolagetStoreObjectDTO> getButikerList() {
		return this.butiker;
	}
	
	

	public String getNamn() {
		return Namn;
	}

	public String getAddress1() {
		return Address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public String getAddress3() {
		return Address3;
	}

	public String getAddress4() {
		return Address4;
	}

	public String getAddress5() {
		return Address5;
	}

	public String getTelefon() {
		return Telefon;
	}

	public String getButiksTyp() {
		return ButiksTyp;
	}

	public String getTjänster() {
		return Tjänster;
	}

	public String getOppettider() {
		return Oppettider;
	}

	public String getRT90x() {
		return RT90x;
	}

	@Override
	public String toString() {
		return "DomainButik [butiker=" + butiker + ", Typ=" + Typ + ", Nr=" + Nr + ", Namn=" + Namn + ", Address1="
				+ Address1 + ", Address2=" + Address2 + ", Address3=" + Address3 + ", Address4=" + Address4
				+ ", Address5=" + Address5 + ", Telefon=" + Telefon + ", ButiksTyp=" + ButiksTyp + ", Tjänster="
				+ Tjänster + ", sokOrd=" + SokOrd + ", OppetTider=" + Oppettider + ", RT90x=" + RT90x + ", RT90y="
				+ RT90y + "]";
	}

}
