package se.yrgo.drinkspiration.model;

import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "artiklar")
@XmlAccessorType(XmlAccessType.FIELD)
public class SystembolagetAssortmentObjectDTO {
	
	private String nr = "";
	private String Artikelid = "";
	private String Varnummer = "";
	private String Namn = "";
	private String Namn2 = "";
	private String Prisinklmoms = "";
	private String Volymiml = "";
	private String PrisPerLiter = "";
	private String Saljstart = "";
	private String Utgått = "";
	private String Varugrupp = "";
	private String Typ = "";
	private String Stil = "";
	private String Forpackning = "";
	private String Forslutning = "";
	private String Ursprung = "";
	private String Ursprunglandnamn = "";
	private String Producent = "";
	private String Leverantor = "";
	private String Argang = "";
	private String Provadargang = "";
	private String Alkoholhalt = "";
	private String sortiment = "";
	private String SortimentText = "";
	private String Ekologisk = "";
	private String etiskt = "";
	private String Koscher = "";
	private String RavarorBeskrivning = "";
	
	@XmlElement(name = "artikel")
	private List<SystembolagetAssortmentObjectDTO> sortimentList;
	
	public List<SystembolagetAssortmentObjectDTO> getSortimentList() {
		return sortimentList;
	}

	public String getNr() {
		return nr;
	}

	public String getArtikelid() {
		return Artikelid;
	}

	public String getVarnummer() {
		return Varnummer;
	}

	public String getNamn() {
		return Namn;
	}

	public String getNamn2() {
		return Namn2;
	}

	public String getPrisinklmoms() {
		return Prisinklmoms;
	}

	public String getVolymiml() {
		return Volymiml;
	}

	public String getPrisPerLiter() {
		return PrisPerLiter;
	}

	public String getSaljstart() {
		return Saljstart;
	}

	public String getUtgått() {
		return Utgått;
	}

	public String getVarugrupp() {
		return Varugrupp;
	}

	public String getTyp() {
		return Typ;
	}

	public String getStil() {
		return Stil;
	}

	public String getForpackning() {
		return Forpackning;
	}

	public String getForslutning() {
		return Forslutning;
	}

	public String getUrsprung() {
		return Ursprung;
	}

	public String getUrsprunglandnamn() {
		return Ursprunglandnamn;
	}

	public String getProducent() {
		return Producent;
	}

	public String getLeverantor() {
		return Leverantor;
	}

	public String getArgang() {
		return Argang;
	}

	public String getProvadargang() {
		return Provadargang;
	}

	public String getAlkoholhalt() {
		return Alkoholhalt;
	}
	
	public String getSortiment() {
		return this.sortiment;
	}

	public String getSortimentText() {
		return SortimentText;
	}

	public String getEkologisk() {
		return Ekologisk;
	}

	public String getEtiskt() {
		return etiskt;
	}

	public String getKoscher() {
		return Koscher;
	}

	public String getRavarorBeskrivning() {
		return RavarorBeskrivning;
	}	
	
}
