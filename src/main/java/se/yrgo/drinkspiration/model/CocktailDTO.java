package se.yrgo.drinkspiration.model;

public class CocktailDTO {
	
	private Long id;
	private String name;
	private String description;
	
	
		
	
	public CocktailDTO() {
		super();
	}

	public CocktailDTO(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CocktailDTO [name=" + name + ", description=" + description + "]";
	}
	
	
	
	

}
