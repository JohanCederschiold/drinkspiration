package se.yrgo.drinkspiration.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SystembolagetAssortmentObject {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nr = "";
	private String Artikelid = "";
	private String Varnummer = "";
	private String Namn = "";
	private String Namn2 = "";
	private String Prisinklmoms = "";
	private String Volymiml = "";
	private String PrisPerLiter = "";
	private String Saljstart = "";
	private String Utgått = "";
	private String Varugrupp = "";
	private String Typ = "";
	private String Stil = "";
	private String Forpackning = "";
	private String Forslutning = "";
	private String Ursprung = "";
	private String Ursprunglandnamn = "";
	private String Producent = "";
	private String Leverantor = "";
	private String Argang = "";
	private String Provadargang = "";
	private String Alkoholhalt = "";
	private String sortiment = "";
	private String SortimentText = "";
	private String Ekologisk = "";
	private String etiskt = "";
	private String Koscher = "";
	private String RavarorBeskrivning = "";
	
	public SystembolagetAssortmentObject() {
		
	}
	
	public SystembolagetAssortmentObject(SystembolagetAssortmentObjectDTO ds) {
		super();
		this.nr = ds.getNr();
		this.Artikelid = ds.getArtikelid();
		this.Varnummer = ds.getVarnummer();
		this.Namn = ds.getNamn();
		this.Namn2 = ds.getNamn2();
		this.Prisinklmoms = ds.getPrisinklmoms();
		this.Volymiml = ds.getVolymiml();
		this.PrisPerLiter = ds.getPrisPerLiter();
		this.Saljstart = ds.getSaljstart();
		this.Utgått = ds.getUtgått();
		this.Varugrupp = ds.getVarugrupp();
		this.Typ = ds.getTyp();
		this.Stil = ds.getStil();
		this.Forpackning = ds.getForpackning();
		this.Forslutning = ds.getForslutning();
		this.Ursprung = ds.getUrsprung();
		this.Ursprunglandnamn = ds.getUrsprunglandnamn();
		this.Producent = ds.getProducent();
		this.Leverantor = ds.getLeverantor();
		this.Argang = ds.getArgang();
		this.Provadargang = ds.getProvadargang();
		this.Alkoholhalt = ds.getAlkoholhalt();
		this.sortiment = ds.getSortiment();
		this.SortimentText = ds.getSortimentText();
		this.Ekologisk = ds.getEkologisk();
		this.etiskt = ds.getEtiskt();
		this.Koscher = ds.getKoscher();
		this.RavarorBeskrivning = ds.getRavarorBeskrivning();
	}
	
	

}
