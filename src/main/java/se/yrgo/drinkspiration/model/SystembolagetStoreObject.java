package se.yrgo.drinkspiration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class SystembolagetStoreObject {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String Typ = "";
	private String Nr = "";
	private String Namn = "";
	private String Address1 = "";
	private String Address2 = "";
	private String Address3 = "";
	private String Address4 = "";
	private String Address5 = "";
	private String Telefon = "";
	private String ButiksTyp = "";
	private String Tjänster = "";
	private String SokOrd = "";
	@Column(length = 100000)
	private String Oppettider = "";
	private String RT90x = "";
	private String RT90y = "";

	

	public SystembolagetStoreObject(SystembolagetStoreObjectDTO storeDTO) {
		super();
		this.Typ = storeDTO.getTyp();
		Nr = storeDTO.getNr();
		Namn = storeDTO.getNamn();
		Address1 = storeDTO.getAddress1();
		Address2 = storeDTO.getAddress2();
		Address3 = storeDTO.getAddress3();
		Address4 = storeDTO.getAddress4();
		Address5 = storeDTO.getAddress5();
		Telefon = storeDTO.getTelefon();
		ButiksTyp = storeDTO.getButiksTyp();
		Tjänster = storeDTO.getTjänster();
		SokOrd = storeDTO.getSokOrd();
		Oppettider = storeDTO.getOppettider();
		RT90x = storeDTO.getRT90x();
		RT90y = storeDTO.getRT90y();
	}

	public String getTyp() {
		return Typ;
	}

	public String getNr() {
		return Nr;
	}

	public void setNr(String nr) {
		Nr = nr;
	}

	public String getSokOrd() {
		return SokOrd;
	}

	public void setSokOrd(String sokOrd) {
		this.SokOrd = sokOrd;
	}

	public String getRT90y() {
		return RT90y;
	}

	public void setRT90y(String rT90y) {
		RT90y = rT90y;
	}

	@Override
	public String toString() {
		return "DomainButik [Typ=" + Typ + ", Nr=" + Nr + ", Namn=" + Namn + ", Address1=" + Address1 + ", Address2="
				+ Address2 + ", Address3=" + Address3 + ", Address4=" + Address4 + ", Address5=" + Address5
				+ ", Telefon=" + Telefon + ", ButiksTyp=" + ButiksTyp + ", Tjänster=" + Tjänster + ", sokOrd=" + SokOrd
				+ ", OppetTider=" + Oppettider + ", RT90x=" + RT90x + ", RT90y=" + RT90y + "]";
	}
}
