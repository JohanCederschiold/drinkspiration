package se.yrgo.drinkspiration.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class CocktailIngredient {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private int amount;
	
	@OneToOne
	private Ingredient ingredient;

	
	public CocktailIngredient() {
		super();
	}

	public CocktailIngredient(int amount, Ingredient ingredient, Volume volume) {
		super();
				
		this.amount = convertToBaseunits(amount, volume);
		this.ingredient = ingredient;

	}
	
	private int convertToBaseunits (int amount, Volume volumeUnit) {
		return amount * volumeUnit.getMilliliters();
	}

	@Override
	public String toString() {
		return "CocktailIngredient [id=" + id + ", amount=" + amount + ", ingredient=" + ingredient + "]";
	}

	public Long getId() {
		return id;
	}

	public int getAmount() {
		return amount;
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

}
