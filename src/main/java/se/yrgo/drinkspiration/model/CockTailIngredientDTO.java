package se.yrgo.drinkspiration.model;

import javax.persistence.OneToOne;

public class CockTailIngredientDTO {
	
	private Long id;
	private int amount;
	private Ingredient ingredient;
	
	
	
	public CockTailIngredientDTO() {

	}

	public CockTailIngredientDTO(Long id, int amount, Ingredient ingredient) {
		this.id = id;
		this.amount = amount;
		this.ingredient = ingredient;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}
	
	
	
	

}
