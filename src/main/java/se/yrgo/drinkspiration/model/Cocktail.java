package se.yrgo.drinkspiration.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Cocktail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(unique=true)
	private String name;
	private String description;
	
	@OneToMany()
	@JoinColumn(name = "cocktail_fk")
	private Set<CocktailIngredient> cocktailIngredient;
	
	
	public Cocktail() {
		super();
		instantiateSet();
	}


	public String getName() {
		return name;
	}
	
	public Long getId() {
		return this.id;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Set<CocktailIngredient> getCocktailIngredient() {
		return cocktailIngredient;
	}


	public void addCocktailIngredient(CocktailIngredient cocktailIngredient) {
		this.cocktailIngredient.add(cocktailIngredient);
	}
	
	
	private void instantiateSet() {
		this.cocktailIngredient = new HashSet<CocktailIngredient>();
	}


	@Override
	public String toString() {
		return "Cocktail [id=" + id + ", name=" + name + ", description=" + description + ", cocktailIngredient="
				+ cocktailIngredient + "]";
	}
	
	
	
	
	
	

}
