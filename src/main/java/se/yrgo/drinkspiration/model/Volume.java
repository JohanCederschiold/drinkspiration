package se.yrgo.drinkspiration.model;

public enum Volume {
	
	ML(1, "mililiters"),
	DL(100, "deciliters"),
	CL(10, "centiliters" ),
	L(1000, "liters");
	
	
	private int milliliters;
	private String volumeName;
	
	
	private Volume(int milliliters, String volumeName) {
		this.milliliters = milliliters;
		this.volumeName = volumeName;
	}


	public int getMilliliters() {
		return milliliters;
	}


	public String getVolumeName() {
		return volumeName;
	}
	
	

	
	
	

}
