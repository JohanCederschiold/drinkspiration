package se.yrgo.drinkspiration;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URL;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import se.yrgo.drinkspiration.model.SystembolagetStoreObjectDTO;
import se.yrgo.drinkspiration.service.SystembolagetStoreService;

public class SystembolagetStoreServiceImplTests {
	
//	SystembolagetStoreServiceImpl storeService = new SystembolagetStoreServiceImpl();
	
	@Autowired
	SystembolagetStoreService storeService;
	
	@Test
	void testURL() {
		storeService.saveStoreListToDatabase(new ArrayList<>());
		assertTrue(storeService.getStoreURL()!= null);
	}
	
//	@Test
//	void testParseXml() {
//		URL url = storeService.getStoreURL();
//		SystembolagetStoreObjectDTO assortmentDTO = storeService.parseXml(url);
//		assertTrue(assortmentDTO != null);
//	}

}
