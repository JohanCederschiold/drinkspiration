package se.yrgo.drinkspiration;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URL;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import se.yrgo.drinkspiration.model.SystembolagetAssortmentObjectDTO;
import se.yrgo.drinkspiration.service.AssortmentService;

@SpringBootTest
public class AssortmentServiceImplTests {

//	private AssortmentServiceImpl assortmentServiceImpl = new AssortmentServiceImpl();
	
	@Autowired
	private AssortmentService assortmentService;

	@Test
	void testURL() {

		assertTrue(assortmentService.getAssortmentURL() != null);
	}

//	@Test
//	void testParseXml() {
//		URL url = assortmentService.getAssortmentURL();
//		SystembolagetAssortmentObjectDTO assortmentDTO = assortmentService.parseXml(url);
//		assertTrue(assortmentDTO != null);
//	}
}
